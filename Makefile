run: install
	cd backend && go run .
install: backend/go.sum
backend/go.sum:
	cd backend && go get
fmt:
	cd backend && go fmt && gofumpt -extra -l .
