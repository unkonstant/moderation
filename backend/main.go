package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"github.com/go-chi/render"
	"github.com/go-ini/ini"

	"codeberg.org/codeberg/moderation/internal/mailer"
)

/* order of file headers
 * - imports
 * - globals, consts
 * - type definitions
 * - routes
 * - config function
 */

/* common globals
 * globals that are only accessed in a single file are defined there
 * using a namespace alike filenameConstantname
 */
var giteaClient *gitea.Client

var (
	jwtAuth  *jwtauth.JWTAuth
	giteaURL string
)

type PermissionLevel int

const (
	PERMISSION_USER      PermissionLevel = 1 // nolint:deadcode
	PERMISSION_MEMBER    PermissionLevel = 2 // nolint:deadcode
	PERMISSION_MODERATOR PermissionLevel = 3
	PERMISSION_ADMIN     PermissionLevel = 4
)

type GenericRequest struct {
	ExpandRepoActionToForks    bool   `json:"expandforks"`
	InformUser                 bool   `json:"informuser"`
	InformUserAdditionalReason string `json:"informuserreason"`
	InternalActionComment      string `json:"internalcomment"`
}

type GenericError struct {
	Action string
	Result string
}

func (err *GenericError) Error() string {
	return fmt.Sprintln(err.Action + ": " + err.Result)
}

func main() {
	// configuration
	cfg, err := ini.Load("moderation_backend.ini")
	if err != nil {
		fmt.Printf("Failed to read file: %v\n", err)
		os.Exit(1)
	}

	appListenUrl := cfg.Section("moderation_app").Key("LISTEN_URL").String()
	if appListenUrl == "" {
		fmt.Printf("Please make sure you have a listen url in your moderation_backend.ini\n")
		os.Exit(2)
	}

	giteaToken := cfg.Section("gitea").Key("API_TOKEN").String()
	giteaURL = cfg.Section("gitea").Key("URL").String()
	if giteaToken == "" || giteaURL == "" {
		fmt.Printf("Please make sure you have token and url in your moderation_backend.ini\n")
		os.Exit(2)
	}
	jwtSecret := cfg.Section("security").Key("JWT_SECRET").String()
	if jwtSecret == "" || jwtSecret == "default_jwt_secret" {
		fmt.Printf("Please make sure you set a unique jwt secret in the moderation_backend.ini\n")
		os.Exit(2)
	}
	userMap := make(map[string]string)
	for _, user := range cfg.Section("security.users").Keys() {
		userMap[user.Name()] = user.String()
	}
	if len(userMap) == 0 {
		fmt.Printf("Warning: You should add at least one user under the [user] section in moderation_backend.ini\n")
	}
	mailer.MailConfig(cfg)
	userConfig(cfg)
	repoConfig(cfg)

	// inits
	giteaClient, err = gitea.NewClient(giteaURL, gitea.SetToken(giteaToken))
	if err != nil {
		fmt.Printf("could not connect to %s\n", giteaURL)
		os.Exit(3)
	}
	jwtAuth = jwtauth.New("HS256", []byte(jwtSecret), nil)

	// chi routing etc
	flag.Parse()

	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(jwtauth.Verifier(jwtAuth))

	r.Route("/users", userRoutes)
	r.Route("/repos", repoRoutes)
	r.Group(func(r chi.Router) {
		r.Use(middleware.BasicAuth("user", userMap))
		r.Get("/auth/login", login)
	})

	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "../frontend"))

	r.Get("/*", func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(filesDir))
		fs.ServeHTTP(w, r)
	})

	fmt.Printf("Starting app now on %s\n", appListenUrl)
	if err := http.ListenAndServe(appListenUrl, r); err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	_, newToken, _ := jwtAuth.Encode(map[string]interface{}{"p": PERMISSION_ADMIN})
	_, _ = w.Write([]byte(newToken))
}

func authRequire(ctx context.Context, requiredPermissionLevel PermissionLevel) bool {
	_, claims, err := jwtauth.FromContext(ctx)
	if err != nil {
		fmt.Printf("DEBUG: error parsing jwtauth: %v\n\n", err)
		return false
	}
	permissionLevel := int(claims["p"].(float64))
	return PermissionLevel(permissionLevel) >= requiredPermissionLevel
}

func filterInto(source, targetStruct interface{}) (interface{}, error) {
	sourceSlice := reflect.ValueOf(source)
	targetType := reflect.TypeOf(targetStruct)
	targetVal := reflect.ValueOf(targetStruct)
	if sourceSlice.Kind() != reflect.Ptr || targetType.Kind() != reflect.Ptr {
		return nil, &GenericError{Result: "No pointer"}
	}
	sourceSlice = sourceSlice.Elem()
	targetType = targetType.Elem()
	targetVal = targetVal.Elem()
	if sourceSlice.Kind() != reflect.Slice || targetType.Kind() != reflect.Struct || targetVal.Kind() != reflect.Struct {
		return nil, &GenericError{Result: "No slice"}
	}
	if sourceSlice.Len() == 0 {
		return nil, nil
	}

	var targetFields []reflect.StructField
	for i := 0; i < targetType.NumField(); i++ {
		targetFields = append(targetFields, targetType.Field(i))
	}
	var targetSlice []interface{}
	for i := 0; i < sourceSlice.Len(); i++ {
		newStruct := reflect.New(targetType)
		for j := range targetFields {
			structfield := newStruct.Elem().FieldByName(targetFields[j].Name)
			structfield.Set(sourceSlice.Index(i).Elem().FieldByName(targetFields[j].Name))
		}
		targetSlice = append(targetSlice, newStruct.Interface())
	}
	return targetSlice, nil
}
