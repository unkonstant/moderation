package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"code.gitea.io/sdk/gitea"
	"github.com/go-chi/chi/v5"
	"github.com/go-ini/ini"
)

var suspiciousUserKeywords []string

type User struct {
	ID          int64     `json:"id"`
	UserName    string    `json:"login"`
	FullName    string    `json:"full_name"`
	Email       string    `json:"email"`
	AvatarURL   string    `json:"avatar_url"`
	LastLogin   time.Time `json:"last_login"`
	Location    string    `json:"location"`
	Description string    `json:"description"`
}

type UserSimple struct {
	ID       int64  `json:"id"`
	UserName string `json:"login"`
	FullName string `json:"full_name"`
}

type UserActionRequest struct {
	GenericRequest
	List []UserSimple `json:"list"`
}

func userRoutes(r chi.Router) {
	r.Get("/list", userList)
	r.Get("/list/suspicious", userListSuspicious)
	r.Post("/delete", userDelete)
}

func userConfig(cfg *ini.File) {
	userconfig := cfg.Section("users")
	suspiciousUserKeywords = userconfig.Key("SUSPICIOUS_USER_KEYWORDS").Strings(",")
}

func userList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/json")
	if !authRequire(r.Context(), PERMISSION_MODERATOR) {
		_, _ = w.Write([]byte("Unauthorized"))
		return
	}
	var aluo gitea.AdminListUsersOptions
	users, _, _ := giteaClient.AdminListUsers(aluo)
	data, err := filterInto(&users, &UserSimple{})
	if err != nil {
		fmt.Println(err.Error())
	}
	res, err := json.Marshal(data)
	if err == nil {
		_, _ = w.Write([]byte(string(res)))
	}
}

func userListSuspicious(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/json")
	if !authRequire(r.Context(), PERMISSION_MODERATOR) {
		_, _ = w.Write([]byte("Unauthorized"))
		return
	}

	users := []*gitea.User{}
	for i := range suspiciousUserKeywords {
		foundUsers, _, err := giteaClient.SearchUsers(gitea.SearchUsersOption{KeyWord: suspiciousUserKeywords[i]})
		if err != nil {
			_, _ = w.Write([]byte(err.Error()))
		}
		users = append(users, foundUsers...)
		if len(users) >= 100 {
			break
		}
	}
	data, err := filterInto(&users, &User{})
	if err != nil {
		fmt.Println(err.Error())
	}

	res, err := json.Marshal(data)
	if err == nil {
		_, _ = w.Write([]byte(string(res)))
	}
}

func userDelete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/json")
	if !authRequire(r.Context(), PERMISSION_ADMIN) {
		w.WriteHeader(403)
		_, _ = w.Write([]byte("Unauthorized"))
		return
	}

	request := UserActionRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		fmt.Printf("Error decoding JSON: %s\n", err.Error())
		return
	}

	var errors []GenericError
	for _, user := range request.List {
		if _, err := giteaClient.AdminDeleteUser(user.UserName); err != nil {
			errors = append(errors, GenericError{"Delete User", err.Error()})
		}
	}

	res, err := json.Marshal(errors)
	if err == nil {
		_, _ = w.Write([]byte(string(res)))
	}
}
