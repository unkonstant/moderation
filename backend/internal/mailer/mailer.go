package mailer

import (
	"bytes"
	"fmt"
	"text/template"

	"github.com/go-ini/ini"
	"gopkg.in/gomail.v2"
)

var (
	mailDialer    *gomail.Dialer
	mailFromname  string
	mailAlwaysBcc string
	mailGiteaURL  string
	mailGiteaName string
)

func MailConfig(cfg *ini.File) {
	mailconfig := cfg.Section("mail")
	hostname := mailconfig.Key("HOSTNAME").String()
	port := mailconfig.Key("PORT").MustInt()
	username := mailconfig.Key("USERNAME").String()
	password := mailconfig.Key("PASSWORD").String()
	mailFromname = mailconfig.Key("FROMNAME").String()
	mailAlwaysBcc = mailconfig.Key("BCC").String()
	mailDialer = gomail.NewDialer(hostname, port, username, password)

	giteaConfig := cfg.Section("gitea")
	mailGiteaURL = giteaConfig.Key("URL").String()
	mailGiteaName = giteaConfig.Key("NAME").String()
}

func MailSend(toMail, toName, templateName string, templateData map[string]interface{}) {
	mailTemplate, err := template.ParseFiles("templates/mail/" + templateName + ".tmpl")
	if err != nil {
		fmt.Printf("Error reading mail template: %s", err.Error())
	}

	templateData["toName"] = toName
	templateData["giteaURL"] = mailGiteaURL
	templateData["giteaName"] = mailGiteaName

	var mailContent bytes.Buffer
	if err = mailTemplate.Execute(&mailContent, templateData); err != nil {
		fmt.Printf("Error parsing mail template: %s", err.Error())
	}

	mail := gomail.NewMessage()
	mail.SetHeader("From", mailFromname)
	mail.SetHeader("To", toMail)
	if mailAlwaysBcc != "" {
		mail.SetHeader("Bcc", mailAlwaysBcc)
	}
	mail.SetHeader("Subject", "Important notice regarding your content on Codeberg.org")
	mail.SetBody("text/plain", mailContent.String())
	if err = mailDialer.DialAndSend(mail); err != nil {
		fmt.Printf("Error sending email: %s", err.Error())
	}
}
