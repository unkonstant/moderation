function render(list) {
	if (list.type === undefined || list.list === undefined) {
		list.renderTarget.innerHTML = "";
		return;
	}
	renderTable(list);
	elId("cb-output-list").value = list.list.map(
		row => `${row["login"] ? "/"+row["login"] : ""}${row["owner"] ? "/"+row["owner"] : "" }${row["name"] ? "/"+row["name"] : ""}`
	).join("\n");
}

// get generic column names from data
function renderGetColumnNames(data) {
	let columns = [];
	for (i = 0; i < data.length; i++) {
		for (key in data[i]) {
			if (columns.indexOf(key) === -1) {
				columns.push(key);
			}
		}
	}
	return columns;
}

function renderTable(list = output, columns = renderGetColumnNames(list.list)) {
	target = list.renderTarget;
	// add empty header for action buttons
	target.innerHTML = '<table class="table"><tr><th></th>' +
		columns.map(columnName => `<th>${columnName}</th>`).join("") +
		"</tr>" +
		list.list.map((row, i) => '<tr>' +
			renderTableRowActionButtons(i, target) +
			columns.map(columnName => `<td>${renderTableFormatCell(columnName, row[columnName])}</td>`).join("") +
			'</tr>'
		).join("") +
		'</table>';
	renderRegisterButtons(list);
}

function renderTableRowActionButtons(i, target) {
	buttons = [];
	switch (target) {
		case input.renderTarget:
			buttons = ["drop"];
			break;
		case output.renderTarget:
			buttons = ["take", "drop"];
			break;
	}
	return "<td>" + buttons.map(button => `<button class="cb-list-action-${button} btn btn-danger" data-id="${i}">${button.toUpperCase()}</button>`).join("") + "</td>";
}

function renderTableFormatCell(column_name, content) {
	switch (column_name) {
		case "avatar_url":
			return '<img class="avatar" src="' + content + '">';
			break;
		case "last_login":
		case "created":
			date = new Date(content);
			if (date.valueOf() == 0) {
				return "never";
			}
			return '<div title="' + content + '">' + date.getFullYear() + '-' + date.getMonth() + '-' + date.getDay() + '</div>';
			break;
		default:
			return content;
	}
}

function renderRegisterButtons(list) {
	target = list.renderTarget;
	Array.from(target.getElementsByClassName("cb-list-action-take")).forEach(function(button){
		button.addEventListener("click", function(e) {
			if (input.type != output.type) {
				resetList(input);
				input.type = output.type;
			}
			input.list.push(output.list[this.getAttribute("data-id")]);
			render(input);
		});
	});
	Array.from(target.getElementsByClassName("cb-list-action-drop")).forEach(function(button){
		button.addEventListener("click", function(e) {
			list.list.splice(this.getAttribute("data-id"), 1);
			render(list);
		});
	});
}
