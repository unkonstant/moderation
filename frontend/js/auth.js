var jwtToken;

document.getElementById("cb-auth-login").addEventListener("click", function(e){
	auth();
});

let auth = function() {
	fetch('/auth/login').then(response =>{
		return response.text().then(function(text) {
			jwtToken = text;
			document.getElementById("cb-auth").innerHTML = "Logged in :)";
		});
	});
}
